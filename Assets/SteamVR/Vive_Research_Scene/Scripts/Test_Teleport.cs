﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class Test_Teleport : MonoBehaviour {

	public GameObject prefab;
	public Rigidbody attachPoint;
	public float range = 100f;
	public GameObject model;
	public GameObject cameraRig;

	Ray laserRay;
	RaycastHit laserHit;
	LineRenderer laser; 
	int shootable;
	int pickUp;
	Vector3 pos;

	SteamVR_TrackedObject trackedObj;
	FixedJoint joint;

	void Awake()
	{
		trackedObj = GetComponent<SteamVR_TrackedObject>();
		shootable = LayerMask.GetMask ("Shootable");
		pickUp = LayerMask.GetMask ("Pick");
		laser = GetComponent<LineRenderer> ();
	}

	void FixedUpdate()
	{
		var device = SteamVR_Controller.Input((int)trackedObj.index);
		Teleport ();
		if (joint == null && device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
		{
			cameraRig.transform.position = pos;
		}
		else if (joint != null && device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
		{
			
		}
	}

	void Teleport(){
		laser.enabled = true;
		laser.SetPosition (0, transform.position);
		laserRay.origin = transform.position;
		laserRay.direction = transform.forward;

		if (Physics.Raycast (laserRay, out laserHit, range, shootable)) {
			laser.SetPosition (1, laserHit.point);
			pos = laserHit.point;
		} else {
			laser.SetPosition (1, laserRay.origin + laserRay.direction * range);
			pos = cameraRig.transform.position;
		}
	}
}
