﻿using UnityEngine;
using System.Collections;

public class TargetHit : MonoBehaviour {
    public TextMesh text;
    public TextMesh secondary_text;
    public GameObject master;
    int score = 0;

    void Start()
    {
        master = GameObject.FindGameObjectWithTag("Left");
        text = GameObject.FindGameObjectWithTag("Score").GetComponent<TextMesh>();
        secondary_text = GameObject.FindGameObjectWithTag("Score2").GetComponent<TextMesh>();
    }
    // Update is called once per frame
    void OnTriggerEnter(Collider Other)
    {
        if (Other.tag == "Ball")
        {
            score = int.Parse(text.text.Substring(7));
            score++;

            //place the marker where your controller was
            //go.transform.position = attachPoint.transform.position;
            text.text = "Score: " + score.ToString();
            secondary_text.text = "Score: " + score.ToString();

            master.GetComponent<SteamVR_TestThrow>().targetCount--;
            Destroy(this.gameObject);

        }
    }
}
