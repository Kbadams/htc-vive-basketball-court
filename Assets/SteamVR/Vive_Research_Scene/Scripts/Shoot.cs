﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class Shoot : MonoBehaviour {
    public GameObject prefab;
    public Rigidbody attachPoint;
    public float range = 100f;
    public float exitspeed = 100.0f;
    public GameObject model;
    public GameObject cameraRig;
    public float yInc;

    public GameObject slider;
    public GameObject trigger;

    Ray laserRay;
    RaycastHit laserHit;
    LineRenderer laser;
    int shootable;
    int pickUp;
    Vector3 pos;

    SteamVR_TrackedObject trackedObj;
    FixedJoint joint;

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
        shootable = LayerMask.GetMask("Shootable");
        pickUp = LayerMask.GetMask("Pick");
        laser = GetComponent<LineRenderer>();
    }

    void FixedUpdate()
    {
        
        //Teleport();
        
    }

    void Teleport()
    {
        laser.enabled = true;
        laser.SetPosition(0, transform.position);
        laserRay.origin = transform.position;
        laserRay.direction = new Vector3(transform.forward.x, transform.forward.y - yInc, transform.forward.z);

        if (Physics.Raycast(laserRay, out laserHit, range, shootable))
        {
            laser.SetPosition(1, laserHit.point);
            pos = laserHit.point;
        }
        else
        {
            laser.SetPosition(1, laserRay.origin + laserRay.direction * range);
      
        }
    }

    public void fireGun()
    {
        var go = GameObject.Instantiate(prefab);
        
        go.transform.position = model.transform.position;
        go.transform.rotation = model.transform.localRotation;

        Rigidbody rb = go.GetComponent<Rigidbody>();
        rb.velocity = exitspeed * attachPoint.transform.up;

        slider.GetComponent<Slide>().slide();
        
    }

}
