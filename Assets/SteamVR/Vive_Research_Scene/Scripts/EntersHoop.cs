﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class EntersHoop : MonoBehaviour {
	
	public TextMesh text;
	public TextMesh secondary_text;
	public GameObject scoreBoard;
	public GameObject pointMarker;
	public Rigidbody attachPoint;
	public Renderer rend;
	public Material Clear;
	int score = 0;
	int currentTimer = 0;
	int maxTimer = 2;

	void Start() {
		//renderer setup for making a shot visualization
		rend = scoreBoard.GetComponent<Renderer> ();
		rend.material.shader = Shader.Find ("Specular");
		rend.enabled = true;
		//coroutine to control how long the color displays
		StartCoroutine (Score_Blink());
		pointMarker.SetActive (true);
	}
	// Update is called once per frame
	void OnTriggerEnter (Collider Other) {
		//var go = GameObject.Instantiate(pointMarker);

		if (Other.tag == "Ball") {
            //set shot marker active
            //pointMarker.SetActive (true);
            score = int.Parse(text.text.Substring(7));
            currentTimer = 0;
			score++;

			//place the marker where your controller was
			//go.transform.position = attachPoint.transform.position;
			text.text = "Score: " + score.ToString();
			secondary_text.text = "Score: " + score.ToString();

			//change the color of the backboard to show you scored
			rend.material.SetColor ("_Color", Color.red);
		}
	}

	IEnumerator Score_Blink()
	{
		while (true) {
			while(currentTimer < maxTimer)
			{
				currentTimer++;
				yield return new WaitForSeconds(0.5f);
	
			}
			rend.material = Clear;
			yield return null;
		}
	}
}
