﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class SteamVR_TestThrow : MonoBehaviour
{
	public GameObject[] prefab;
	public Rigidbody[] attachPoint;
	public GameObject[] model;
    public GameObject pointer;
    public GameObject pinwheel;
    public GameObject gunTip;
    public GameObject trigger;
    public GameObject target;

    Ray laserRay;
    RaycastHit hit;
    LineRenderer laser;
    LayerMask layerMask = -1;
    Quaternion triggerStartRot, triggerStartRotWorld;
    public int targetCount;

    SteamVR_TrackedObject trackedObj;
	FixedJoint joint;
	int prefabIndex, modelIndex, attachIndex;
    float pointerz, pointery;
    float velx, vely, velz, ballx, bally, ballz;
    public float yInc, yposInc;

	void Awake()
	{
		trackedObj = GetComponent<SteamVR_TrackedObject>();
		prefabIndex = 0;
        modelIndex = 0;
        attachIndex = 0;
        targetCount = 0;
        model[1].SetActive(false);
        pinwheel.SetActive(false);
        triggerStartRot = trigger.transform.localRotation;
        triggerStartRotWorld = trigger.transform.rotation;
    }

	void FixedUpdate()
	{
		var device = SteamVR_Controller.Input((int)trackedObj.index);

        if(modelIndex == 1)
        {
            spawnTarget();
        }

        if (joint == null && device.GetTouch(SteamVR_Controller.ButtonMask.Touchpad))
        {
            pinwheel.SetActive(true);
            pointerz = device.GetState().rAxis0.x * -1/2;
            pointery = device.GetState().rAxis0.y/2;
            pointer.transform.localPosition = new Vector3(-4, pointery, pointerz);
        }
        else
        {
            pointer.transform.localPosition = new Vector3(-4, 0, 0);
            pinwheel.SetActive(false);
        }
        if (joint == null && device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
        {
            //quad 2
            if (device.GetState().rAxis0.x > 0 && device.GetState().rAxis0.y > 0)
            {
                Debug.Log("2");
                prefabIndex = 1;
                modelIndex = 0;
                attachIndex = 0;
                model[modelIndex].SetActive(true);
                
                model[1].SetActive(false);
            }
            //quad 4
            if (device.GetState().rAxis0.x > 0 && device.GetState().rAxis0.y < 0)
            {
                Debug.Log("4");
            }
            //quad 1
            if (device.GetState().rAxis0.x < 0 && device.GetState().rAxis0.y > 0)
            {
                Debug.Log("1");
                prefabIndex = 0;
                modelIndex = 0;
                attachIndex = 0;
                model[modelIndex].SetActive(true);
                model[1].SetActive(false);
            }
            //quad 3
            if (device.GetState().rAxis0.x < 0 && device.GetState().rAxis0.y < 0)
            {
                Debug.Log("3");
                prefabIndex = 2;
                modelIndex = 1;
                attachIndex = 1;
                model[modelIndex].SetActive(true);
                model[0].SetActive(false);
            }
        }

        if (joint == null && device.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
        {
            if (modelIndex == 1)
            {
                trigger.transform.localRotation = new Quaternion(triggerStartRot.x + (device.GetState().rAxis1.x/2), triggerStartRot.y, triggerStartRot.z, triggerStartRot.w);
            }
        }
        else
        {
            trigger.transform.localRotation = triggerStartRot;
            
        }

        if (joint == null && device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
		{
            if (modelIndex == 0)
            {
                var go = GameObject.Instantiate(prefab[prefabIndex]);

                go.transform.position = model[modelIndex].transform.position;
                go.transform.rotation = model[modelIndex].transform.rotation;

                joint = go.AddComponent<FixedJoint>();
                joint.connectedBody = attachPoint[attachIndex];
            }
            else if(modelIndex == 1)
            {
                this.GetComponentInChildren<Shoot>().fireGun();
            }
           
		}
		else if (joint != null && device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
		{
			var go = joint.gameObject;
			var rigidbody = go.GetComponent<Rigidbody>();
			Object.DestroyImmediate(joint);
			joint = null;
			Object.Destroy(go, 45.0f);

			var origin = trackedObj.origin ? trackedObj.origin : trackedObj.transform.parent;
			if (origin != null)
			{
                velx = origin.TransformVector(device.velocity).x*7.5f;
                vely = origin.TransformVector(device.velocity).y*5;
                velz = origin.TransformVector(device.velocity).z*7.5f;
                rigidbody.velocity = new Vector3(velx, vely, velz);
				rigidbody.angularVelocity = origin.TransformVector(device.angularVelocity);
			}
			else
			{
                velx = device.velocity.x;
                vely = device.velocity.y;
                velz = device.velocity.z;
                rigidbody.velocity = new Vector3(velx, vely, velz);
				rigidbody.angularVelocity = device.angularVelocity;
			}

			rigidbody.maxAngularVelocity = rigidbody.angularVelocity.magnitude;
		}
	}

    void spawnTarget()
    {
        if(targetCount < 10)
        {
            Vector3 position = new Vector3(Random.Range(-16.0f, 16.0f), Random.Range(1f, 5f), Random.Range(-8.0f, 8.0f));
            var go = GameObject.Instantiate(target, position, target.transform.rotation);
            targetCount++;
        }
    }
}
